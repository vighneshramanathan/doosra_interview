var express = require('express');
var app = express();

app.get('/cabDriverList', function (req, res) {
    return [{
        cabDriverID: 1,
        name: 'driver1',
        currentLatitude: '12.922489086064326',
        currentLongitude: '80.15256277841303',
        rideStatus: 0,
        carDetails:{
            registeredNumber:'TN 11 2222',
            contactNumber:'2123456789'
        }
    }, {
        cabDriverID: 2,
        name: 'driver2',
        currentLatitude: '12.92384304687676',
        currentLongitude: '80.11794004699459',
        rideStatus: 1,
        carDetails:{
            registeredNumber:'TN 11 2222',
            contactNumber:'2123456789'
        }
    }]
})

app.post('/loginUser', function (req, res) {
    let userInfo = req.body;
    // instead of db validation, hardcoding the mobile no
    if(userInfo.mobile == "9841286112"){
        if(userInfo.loginType=="otp"){
            // OTP SMS Gateway validation and email OTP validation from database
            return{
                success:true,
                userDeatils:[{
                    name: 'user1',
                    mobileNumber: '123455',
                    emailID: 'op@da.com',
                    userID: 1
                }]
            }
        }else if(userInfo.loginType=="password"){
            // based on the encryption we can do the hash tag
            return{
                success:true,
                userDeatils:[{
                    name: 'user1',
                    mobileNumber: '123455',
                    emailID: 'op@da.com',
                    userID: 1
                }]
            }
        }
    }else{
        return{
            success: false,
            msg: 'User is not registered!!!'
        }
    }
});

app.post('/carDriverBooking/otpVerification',function(req,res){
    // otp validation before starting the cab
    let request = res.body;
    // fetch the booking details from table with request.bookingID
    if(request.bookingOtp='345'){
        // Update in Booking status in table as rideStatus: 'OnGoing'
        return{
            success:true,
            msg: 'Ride Started & navigation happens',
            bookingDetails: [{
                bookingID: '123',
                otp: '345',
                userID: '1',
                source:{lat: '12.922489086064326', lon: '80.15256277841303'},
                destination:{lat: '12.92384304687676', lon: '80.11794004699459'},
                driverDeatils:result.driverDeatils,
                rideStatus: 'OnGoing',
                paymentStatus: 'NA',
                paymentMode:'Cash',
                finalAmount: '233.00'
            }]
        }
    }else{
        return{
            success:false,
            msg: 'Invalid OTP!!!'
        }
    }
})

app.put('/driveCloseBooking', function(req,res){
    // Update in Booking status in table as rideStatus: 'Completed', paymentStatus: 'Success', paymentMode:'Cash'
    return{
        success: true,
        msg: 'Continue with other ride!!!'
    }
    // customer will get the feedBack screen once the booking is completed
});

app.post('/userBooking', async function (req, res) {

    let bookingDetails = req.body;
    let result = await checkCurrentLocRadius(bookingDetails.lat, bookingDetails.lon);

    if (result.availability == true) {
        let bookingInfo = {
            bookingID: '123',
            otp: '345',
            userID: '1',
            source:{lat: '12.922489086064326', lon: '80.15256277841303'},
            destination:{lat: '12.92384304687676', lon: '80.11794004699459'},
            driverDeatils:result.driverDeatils,
            rideStatus: 'Confirmed',
            paymentStatus: 'NA',
            paymentMode:'Cash',
            finalAmount: '233.00'
        }
        return {
            success: true,
            bookingInformation: bookingInfo
        }
    }else{
        return {
            success: false,
            msg: 'no Cab driver found within your loaction!!!'
        }
    }

})

var server = app.listen(9000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("http://%s:%s", host, port)
})

function checkCurrentLocRadius(lat, lon) {
    // checking lat & lon radius. As of now iam hardcoding values
    if (lat & lon) {
        // fetching driver details from DB, instead of DB fetching hardcoding
        let driverList = [{
            cabDriverID: 1,
            name: 'driver1',
            currentLatitude: '12.922489086064326',
            currentLongitude: '80.15256277841303',
            rideStatus: 0,
            carDetails:{
                registeredNumber:'TN 11 2222',
                contactNumber:'2123456789'
            }
        }, {
            cabDriverID: 2,
            name: 'driver2',
            currentLatitude: '12.92384304687676',
            currentLongitude: '80.11794004699459',
            rideStatus: 1,
            carDetails:{
                registeredNumber:'TN 11 2222',
                contactNumber:'2123456789'
            }
        }]
        return {
            driverDeatils: driverList[0],
            availability: true
        };
    } else {
        return {
            availability:false
        };
    }
}